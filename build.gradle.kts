import org.jetbrains.compose.compose
import org.jetbrains.compose.desktop.application.dsl.TargetFormat
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.5.21"
    id("org.jetbrains.compose") version "0.5.0-build270"
}

group = "me.lcj34"
version = "1.0"

repositories {
    jcenter()
    mavenCentral()
    maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    maven("https://maven.pkg.jetbrains.space/data2viz/p/maven/public")
}

dependencies {
    implementation(kotlin("stdlib-common"))
    implementation(compose.desktop.currentOs)
    implementation(compose.runtime)
    implementation(compose.desktop.common)
    implementation("net.maizegenetics:tassel6:6.0.1")
    implementation("io.data2viz.d2v:color:0.8.13")
    implementation("io.data2viz.d2v:random:0.8.13")
    implementation("io.data2viz.d2v:shape:0.8.13")
    implementation("io.data2viz.d2v:scale:0.8.13")
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "11"
}

compose.desktop {
    application {
        mainClass = "MainKt"
        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "phg_compose_desktop"
            packageVersion = "1.0.0"
        }
    }
}