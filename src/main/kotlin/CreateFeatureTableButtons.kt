import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.desktop.Window
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import net.maizegenetics.analysis.brapi.BrAPIConnection
import net.maizegenetics.brapp.hapviewer.HaplotypeGrid
import net.maizegenetics.dna.factor.FeatureTable
import net.maizegenetics.dna.factor.io.BuilderFromHaplotypeVCF

// This file takes the brapp.hapviewer.HapltoypeGrid.kt code and
// tries to get it to create a haplotype grid based on user input
// from buttons.  I am not yet able to get it on the same screen -
// here it goes to a separate window.
// And .. killing the separate window kills the whole application - I"d
// like to be able to kill the new window without killing the program
@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun featureTableFromButtons() {

    val formatOptions = listOf("VCF File", "PHG Methods")
    var selectedIndex by remember { mutableStateOf(0) }
    var selectedFormat by remember { mutableStateOf("") }
    var selectedText by remember { mutableStateOf("VCF File")}

    var featureTableValue = remember { mutableStateOf("") }
    var featureTable: FeatureTable? by remember { mutableStateOf(null) } // this allows haplotypeGrid to appear in main screen
    var featureTableValueReset = remember {mutableStateOf(false)}

    val loading = mutableStateOf(false)

    Column {

        Spacer(modifier = Modifier.padding(20.dp))
        Row {
            Spacer(modifier = Modifier.padding(10.dp))
            // THis is the box where user types values:  path to vcf file or comma separated list of methods
            TextField(
                value = featureTableValue.value,
                onValueChange = { newValue ->
                    featureTableValue.value = newValue
                },
                label = { Text("Feature Table Input") },
                modifier = Modifier.align(Alignment.CenterVertically).size(300.dp, 50.dp)
            )

            // This is the drop-down option, identifying if text is VCF file or method list
            Spacer(modifier = Modifier.padding(10.dp))
            var expanded by remember { mutableStateOf(false) }

            // Begin OutlinedTExt version of drop down
            //  https://stackoverflow.com/questions/67111020/exposed-drop-down-menu-for-jetpack-compose
            val icon = if (expanded) {
                // only the icon changes, not the data for the dropdown
                Icons.Filled.ArrowForward
            } else {
                Icons.Filled.ArrowDropDown
            }

            // THe column here lines up the drop-down menu options with the drop-down button.
            // without it, the options appear at the far left.
            Column() {
                OutlinedTextField(
                    value = selectedText,
                    onValueChange = { selectedText = it },
                    //modifier = Modifier.fillMaxWidth(),
                    label = {Text("Feature Table Input Option")},
                    trailingIcon = {
                        Icon(Icons.Default.ArrowDropDown, contentDescription = null, modifier = Modifier.clickable { expanded = !expanded })
                        //Icon(icon,"contentDescription", Modifier.clickable { expanded = !expanded })
                    }
                )
                DropdownMenu(
                    expanded = expanded,
                    onDismissRequest = { expanded = false },
                    modifier = Modifier.background(Color.LightGray).align(Alignment.CenterHorizontally).weight(2f)
                ) {
                    formatOptions.forEach { label ->
                        DropdownMenuItem(onClick = {
                            selectedText = label
                            println("in onclick - label=${label}, selectedText=${selectedText}")
                        }) {
                            Text(text = label)
                            println("After suggestions in DropdownMenu: label= ${label}")
                        }
                    }
                }
            }
            // This is a button that will take the value from the user input text box, and the
            // value from the format dropdown, and create the FeatureTable for HaplotypeGrid
            // Needs values of : format and featureTableValue
            Spacer(modifier = Modifier.padding(5.dp))
            val openDialog = remember { mutableStateOf(false) }

            Button(modifier = Modifier.align(Alignment.CenterVertically),
                onClick = {
                    openDialog.value = true // what does this line do? copied from somewhere
                    println("Value of featureTableValue: ${featureTableValue.value}")
                    println("Value of index = ${selectedIndex}, Value of format: ${selectedText}")

                    //if (progress < 1f) progress += 0.1f
                    if (selectedText.equals(formatOptions[0])) {
                        println("calling BuilderFromHaplotypeVCF")
                        featureTable = BuilderFromHaplotypeVCF().read(featureTableValue.value)
                        featureTableValueReset.value = true  // indicate this has changed
                    } else {

                        //val connection = BrAPIConnection("http://cbsudc01.biohpc.cornell.edu/brapi/v2")
                        val connection = BrAPIConnection("http://localhost:8080/brapi/v2")
                        // THe connection is stable, it is the db accessed through this connection that uses
                        // either phgsmallseq_test (for testing) or maize_1_0 when initial tests work)
                        //
                        // LCJ - for phg_webktor_service, you have the application.conf file indicating
                        // the db for connection.  This happens when run "main" to setup the connection.
                        // SO the connection on dc01 is to maize_1_0.  You need to use the connection on
                        // your iMac to get o smallseq

                        println("getting brapi connection, featureTableValue is ${featureTableValue.value}")
                        //featureTable = connection.getCalls("HAP_PATH_METHOD")
                        featureTable = connection.getCalls(featureTableValue.value)

                        println("num of features: ${featureTable!!.numFeatures()}")
                        println("num of taxa: ${featureTable!!.numTaxa()}")

                    }

                })
            {
                Text("create HaplotypeGrid")
            } // end button
        } // end row
        Row {
            // Works outside the conditional, but because we have no data in the feature table,
            // it only prints the first Scale scroll bar
            HaplotypeGrid(featureTable) // this works outside the conditional
        }

    } // end column
}

@Composable
fun SimpleCircularProgressIndicator(isDisplayed: Boolean) {
    if (isDisplayed) {
        Row (modifier = Modifier.fillMaxWidth().padding(60.dp),
            horizontalArrangement = Arrangement.Center
        ){
            CircularProgressIndicator(color= MaterialTheme.colors.primary)
        }
    }

}
