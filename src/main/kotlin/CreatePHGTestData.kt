fun createTestRefRangeList(): List<ReferenceRange> {
    val refRangeList = mutableListOf<ReferenceRange>()

    val range208 = ReferenceRange(208,"1",4173806,4249156)
    refRangeList.add(range208)
    val range9541 = ReferenceRange(9541,"2",13086481,13088962)
    refRangeList.add(range9541)
    val range17075 = ReferenceRange(17075, "3",52194389,52194389)
    refRangeList.add(range17075)
    val range22968 = ReferenceRange(22968,"4",38326038,38327476)
    refRangeList.add(range22968)
    val range30324 = ReferenceRange(30324,"5",63375981,63376941)
    refRangeList.add(range30324)

    val range2464 = ReferenceRange(2464,"1",63770254,63773278)
    refRangeList.add(range2464)
    val range11551 = ReferenceRange(11551,"2",87125031,87137305)
    refRangeList.add(range11551)
    val range18141 = ReferenceRange(18141, "3",136899961,136904506)
    refRangeList.add(range18141)
    val range24280 = ReferenceRange(24280,"4",127377600,127382081)
    refRangeList.add(range24280)
    val range32620 = ReferenceRange(32620,"5",181396709,181420098)
    refRangeList.add(range32620)
    return refRangeList
}

fun createTestHaplotypesList(): List<Haplotype> {
    val haplotypesList = mutableListOf<Haplotype>()

    val haplotypeB97to208 = Haplotype(76919,"B97","1",4386903,4456110,208)
    haplotypesList.add(haplotypeB97to208)
    val haplotypeCML103to208 = Haplotype(115002,"CML103","1",4255879,4329976,208)
    haplotypesList.add(haplotypeCML103to208)
    val haplotypeHP301to208 = Haplotype( 570184,"HP301","1", 4531525,4597789,208)
    haplotypesList.add(haplotypeHP301to208)
    val haplotypeM162Wto208 = Haplotype(855124,"M162W","1", 4599575,4668809,208)
    haplotypesList.add(haplotypeM162Wto208)
    val haplotypeNC350to208 = Haplotype(1083191,"NC350","1",4281370,4357203,208)
    haplotypesList.add(haplotypeNC350to208)

    val haplotypeB97to2464 = Haplotype(79155,"B97","1",65752171,65754553,2464)
    haplotypesList.add(haplotypeB97to2464)
    val haplotypeCML103to2464 = Haplotype(117232,"CML103","1",66354568,66357410,2464)
    haplotypesList.add(haplotypeCML103to2464)
    val haplotypeHP301to2464 = Haplotype( 572415,"HP301","1", 64357694,64360138,2464)
    haplotypesList.add(haplotypeHP301to2464)
    val haplotypeM162Wto2464 = Haplotype(857360,"M162W","1", 64677541,64680562,2464)
    haplotypesList.add(haplotypeM162Wto2464)
    val haplotypeNC350to2464 = Haplotype(1085425,"NC350","1",65393866,65396573,2464)
    haplotypesList.add(haplotypeNC350to2464)

    val haplotypeB97to9541 = Haplotype(70775,"B97","2",12163664,12168103,9541)
    haplotypesList.add(haplotypeB97to9541)
    val haplotypeCML103to9541 = Haplotype(124220,"CML103","2",13179636,13185731,9541)
    haplotypesList.add(haplotypeCML103to9541)
    val haplotypeHP301to9541 = Haplotype( 579389,"HP301","2", 12698490,12705827,9541)
    haplotypesList.add(haplotypeHP301to9541)
    val haplotypeM162Wto9541 = Haplotype(864359,"M162W","2", 12115168,12117628,9541)
    haplotypesList.add(haplotypeM162Wto9541)
    val haplotypeNC350to9541 = Haplotype(1092404,"NC350","2",13050740,13055069,9541)
    haplotypesList.add(haplotypeNC350to9541)

    val haplotypeB97to11551 = Haplotype(70775,"B97","2",87372732,87388803,11551)
    haplotypesList.add(haplotypeB97to11551)
    val haplotypeCML103to11551 = Haplotype(124220,"CML103","2",86778975,86796648,11551)
    haplotypesList.add(haplotypeCML103to11551)
    val haplotypeHP301to11551 = Haplotype( 579389,"HP301","2", 89081903,89094608,11551)
    haplotypesList.add(haplotypeHP301to11551)
    val haplotypeM162Wto11551 = Haplotype(864359,"M162W","2", 83705078,83721418,11551)
    haplotypesList.add(haplotypeM162Wto11551)
    val haplotypeNC350to11551 = Haplotype(1092404,"NC350","2",87045320,87058022,11551)
    haplotypesList.add(haplotypeNC350to11551)

    val haplotypeB97to17075 = Haplotype(0,"B97","0",0,0,17075)
    haplotypesList.add(haplotypeB97to17075) // B97 was not here
    val haplotypeCML103to17075 = Haplotype(130850,"CML103","3",52436583,52440001,17075)
    haplotypesList.add(haplotypeCML103to17075)
    val haplotypeHP301to17075 = Haplotype( 586003,"HP301","3", 51926700,51930118,17075)
    haplotypesList.add(haplotypeHP301to17075)
    val haplotypeM162Wto17075 = Haplotype(870993,"M162W","3", 52475822,52479241,17075)
    haplotypesList.add(haplotypeM162Wto17075)
    val haplotypeNC350to17075 = Haplotype(1099041,"NC350","3",53386837,53390256,17075)
    haplotypesList.add(haplotypeNC350to17075)

    val haplotypeB97to18141 = Haplotype(70775,"B97","3",136332347,136336893,18141)
    haplotypesList.add(haplotypeB97to18141)
    val haplotypeCML103to18141 = Haplotype(124220,"CML103","3",138530782,138535320,18141)
    haplotypesList.add(haplotypeCML103to18141)
    val haplotypeHP301to18141 = Haplotype( 579389,"HP301","3", 138703020,138707558,18141)
    haplotypesList.add(haplotypeHP301to18141)
    val haplotypeM162Wto18141 = Haplotype(864359,"M162W","3", 137747076,137751621,18141)
    haplotypesList.add(haplotypeM162Wto18141)
    val haplotypeNC350to18141 = Haplotype(1092404,"NC350","3",138493621,138498159,18141)
    haplotypesList.add(haplotypeNC350to18141)

    val haplotypeB97to22968 = Haplotype(65368,"B97","4",37462271,37463708,22968)
    haplotypesList.add(haplotypeB97to22968)
    val haplotypeCML103to22968 = Haplotype(137719,"CML103","4",37852761,37854219,22968)
    haplotypesList.add(haplotypeCML103to22968)
    val haplotypeHP301to22968 = Haplotype( 592878,"HP301","4", 37602410,37603848,22968)
    haplotypesList.add(haplotypeHP301to22968)
    val haplotypeM162Wto22968 = Haplotype(877847,"M162W","4", 37333016,37334454,22968)
    haplotypesList.add(haplotypeM162Wto22968)
    val haplotypeNC350to22968 = Haplotype(1105910,"NC350","4",38126241,38127699,22968)
    haplotypesList.add(haplotypeNC350to22968)

    val haplotypeB97to24280 = Haplotype(65368,"B97","4",129149000,129153497,24280)
    haplotypesList.add(haplotypeB97to24280)
    val haplotypeCML103to24280 = Haplotype(137719,"CML103","4",127356470,127360951,24280)
    haplotypesList.add(haplotypeCML103to24280)
    val haplotypeHP301to24280 = Haplotype( 592878,"HP301","4", 128156927,128161408,24280)
    haplotypesList.add(haplotypeHP301to24280)
    val haplotypeM162Wto24280 = Haplotype(877847,"M162W","4", 128982332,128986829,24280)
    haplotypesList.add(haplotypeM162Wto24280)
    val haplotypeNC350to24280 = Haplotype(1105910,"NC350","4",127960892,127965373,24280)
    haplotypesList.add(haplotypeNC350to24280)

    val haplotypeB97to30324 = Haplotype(86941,"B97","5",63212851,63213805,30324)
    haplotypesList.add(haplotypeB97to30324)
    val haplotypeCML103to30324 = Haplotype(143944,"CML103","5",62742698,62743664,30324)
    haplotypesList.add(haplotypeCML103to30324)
    val haplotypeHP301to30324 = Haplotype( 599096,"HP301","5", 62744992,62745946,30324)
    haplotypesList.add(haplotypeHP301to30324)
    val haplotypeM162Wto30324 = Haplotype(884110,"M162W","5", 63012543,63013506,30324)
    haplotypesList.add(haplotypeM162Wto30324)
    val haplotypeNC350to30324 = Haplotype(1112132,"NC350","5",62073048,62074014,30324)
    haplotypesList.add(haplotypeNC350to30324)

    val haplotypeB97to32620 = Haplotype(86941,"B97","5",177596716,177617271,32620)
    haplotypesList.add(haplotypeB97to32620)
    val haplotypeCML103to32620 = Haplotype(143944,"CML103","5",179203232,179216435,32620)
    haplotypesList.add(haplotypeCML103to32620)
    val haplotypeHP301to32620 = Haplotype( 599096,"HP301","5", 181635119,181659081,32620)
    haplotypesList.add(haplotypeHP301to32620)
    val haplotypeM162Wto32620 = Haplotype(884110,"M162W","5", 179257575,179280961,32620)
    haplotypesList.add(haplotypeM162Wto32620)
    val haplotypeNC350to32620 = Haplotype(1112132,"NC350","5",177795860,177808527,32620)
    haplotypesList.add(haplotypeNC350to32620)

    return haplotypesList
}

// We'll have a column of String.  But how to scroll that sideways?  Does each value in the string need to be a lazy row?
// so we still have lazy row and lazy column, but they are created from this list?
// This assumes all taxa have an entry for each reference range.  If no haplotype, the id is 0, and
// that was created on the list as we did above where B97 was missing a haplotypes at refRange 17075
fun createRefRangeHapListRow( refRangeList:List<ReferenceRange>, haplotypeList:List<Haplotype>):List<String> {

    val lazyRowData = mutableListOf<String>()
    val haplotypeMapByTaxa = haplotypeList.groupBy {it.taxa}
    println("haplotypeMapByTaxa, number of keys = ${haplotypeMapByTaxa.keys.size}")
    val taxaList = haplotypeMapByTaxa.keys.toSet()

    println("Size of refRangeList:${refRangeList.size},size of haplotypeList:${haplotypeList.size}, size of taxaList=${taxaList.size}")

    val headerRow = StringBuilder()
    val taxaListString = taxaList.joinToString("\t")
    headerRow.append("RefRangeID\t${taxaListString}")
    lazyRowData.add(headerRow.toString())
    // other rows are created by appending refRangeId to hapid for each of the hapis in the map
    val haplotypeMapByRefRange = haplotypeList.groupBy{it.refRangeId}
    val rrList = refRangeList
    for (range in refRangeList) {
        val rowSB = StringBuilder()
        rowSB.append(range.myID).append("\t")
        val hapData = haplotypeMapByRefRange.get(range.myID)!!.sortedBy {it.taxa }
        // here I want to append the haplotype id - a string of them, in the same order as the taxa list in the header
        for (haplotype in hapData) {
            rowSB.append(haplotype.hapId).append("\t")
        }
        rowSB.setLength(rowSB.length-1) // remove the last tab character
        lazyRowData.add(rowSB.toString())
    }

    return lazyRowData
}