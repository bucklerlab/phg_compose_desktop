import androidx.compose.runtime.Composable

/**
 * THis class is not yet functional.  Currently printing a PHG matrix from
 * PHGLists.kt
 */
class FeatureTableSimulator (var refRangeList: List<ReferenceRange>, var hapList: List<Haplotype>){
    // THis will be an intArray, as what will be printed in each square is a haplotypeID,
    // which are ints
    var grid: Array<IntArray>

    fun testGrid () {
        // Without being able to add header row/columns, we won't know which ref range
        // or which taxa  is in each swuare.  ALl we have are random haploytpe numbers
        // This really needs to be the lazyCOlumn/LazyRow - but that also had issues
        // See lazyColumn/lazyROw in PHGLists.kt
        val haplotypeMapByRefRange = hapList.groupBy{it.refRangeId}

        for (idx in 0 until refRangeList.size-1) {

            grid[idx][0] = refRangeList[idx].myID // sets the refRangeId

            val hapData = haplotypeMapByRefRange.get(refRangeList[idx].myID)!!.sortedBy {it.taxa }
            for (jdx in 1 until hapList.size) {
                grid[idx][jdx+1] = hapData[jdx-1].hapId
            }
        }
    }

    // LCJ - create this without piece - or at least re-do it
//    @Composable
//    fun printTestGrid() {
//        for (idx in 0 until refRangeList.size-1) {
//            for (jdx in 0 until hapList.size) {
//                val entry = hapList[jdx]
//                val hapid=entry.hapId
//                val chr = entry.asmContig
//                val start = entry.asmStart
//                val end = entry.asmEnd
//                piece(jdx, idx, org.jetbrains.compose.common.core.graphics.Color(51,149,214),hapid,chr,start,end)
//            }
//        }
//    }

    init {
        grid = Array(refRangeList.size) {IntArray(hapList.size)}
    }
}