/**
 * Class defines haplotype data.  But do I also need the reference range to which it maps?
 * WIll need that for this code. SHOuld it be here? Put it here for now - this is just a mock up
 */
class Haplotype(var hapId: Int=0, var taxa: String="0", var asmContig: String="0", var asmStart: Int=0, var asmEnd: Int=0, var refRangeId: Int=0) {

}
