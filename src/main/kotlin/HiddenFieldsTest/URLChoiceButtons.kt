package HiddenFIeldsTest

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import com.google.gson.Gson
import com.google.gson.JsonParser
import net.maizegenetics.analysis.brapi.BrAPIConnection
import net.maizegenetics.brapp.hapviewer.FileChooser
import net.maizegenetics.brapp.hapviewer.HaplotypeGrid
import net.maizegenetics.dna.factor.FeatureTable
import net.maizegenetics.dna.factor.io.BuilderFromHapMap
import net.maizegenetics.dna.factor.io.BuilderFromHaplotypeVCF
import okhttp3.OkHttpClient
import okhttp3.Request

@Composable
fun URLButtons() {
    var expanded by remember { mutableStateOf(false) }
    var filenameOrMethod by remember { mutableStateOf("") }
    var isOpen = remember { mutableStateOf(false) }

    var urlbase by remember {mutableStateOf(TextFieldValue()) }

    var table: FeatureTable? by remember { mutableStateOf(null) }
    var pathList: List<String>? by remember { mutableStateOf(null) }
    var selectedPath: String?  by remember { mutableStateOf(null)}


    if (isOpen.value) {
        FileChooser(
            onCloseRequest = { filename ->
                filename?.let { filenameOrMethod = it }
                isOpen.value = false
                println("Result $filename")
            }
        )
    }

    Column {

        Spacer(modifier = Modifier.padding(20.dp))
        Row(verticalAlignment = Alignment.CenterVertically) {
            Spacer(modifier = Modifier.padding(20.dp))
            Text(modifier= Modifier.background(color = Color.Cyan),text="1. Enter BrAPI URL\n2. Click Query Button to populate PHG Method dropdown menu.\n3. Select PHG method from drop-down.\n" +
                    "4. Click the LOAD button to load the grid")
        }
//        Row(verticalAlignment = Alignment.CenterVertically) {
//            Spacer(modifier = Modifier.padding(20.dp))
//            Text(modifier= Modifier.background(color = Color.Cyan),text="2. Select PHG method from drop-down.\n3. Click the LOAD button to load the grid")
//        }

        Spacer(modifier = Modifier.padding(20.dp))
        Row(verticalAlignment = Alignment.CenterVertically) {

            Spacer(modifier = Modifier.padding(20.dp))

            Spacer(modifier = Modifier.padding(20.dp))
            TextField(
                value = urlbase,
                onValueChange = {value -> urlbase=value},
                label = {Text("BrAPI URL ")}
            )
            Spacer(modifier = Modifier.padding(20.dp))
            Button (
                onClick = {
                    // This list must populate a drop-down menu attached to the Text file
                    // pathList = getPathList(selectedURL)
                    pathList = getPathList(urlbase.text)
                },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color.Blue,
                    contentColor = Color.White
                )
            ){
                Text(
                    text = "Query Method Names",
                    modifier = Modifier.padding(10.dp)
                )
            }
            Spacer(modifier = Modifier.padding(20.dp))
            Column {
                OutlinedTextField(
                    value = filenameOrMethod,
                    onValueChange = { filenameOrMethod = it },
                    label = { Text("Enter PHG Method") },
                    trailingIcon = {
                        Icon(Icons.Default.ArrowDropDown, contentDescription = null, modifier = Modifier.clickable { expanded = !expanded })
                        //Icon(icon,"contentDescription", Modifier.clickable { expanded = !expanded })
                    }
                )
                DropdownMenu(
                    expanded = expanded,
                    onDismissRequest = { expanded = false },
                    modifier = Modifier.background(Color.LightGray).align(Alignment.CenterHorizontally).weight(2f)
                ) {
                    pathList?.forEach { label ->
                        DropdownMenuItem(onClick = {
                            selectedPath = label
                            filenameOrMethod = label // lcj  - was this missing?
                            println("in onclick - label=${label}, selectedText=${selectedPath}, filenameOrMethod=${filenameOrMethod}")
                        }) {
                            Text(text = label)
                            println("After suggestions in PATH DropdownMenu: label= ${label}")
                        }
                    }
                }
            }


            Spacer(modifier = Modifier.padding(20.dp))



//            Column() {
//                OutlinedTextField(
//                    value = selectedURL,
//                    onValueChange = { selectedURL = it },
//                    //modifier = Modifier.fillMaxWidth(),
//                    label = { Text("BrAPI URL ") },
//                )
//
//            }


            Spacer(modifier = Modifier.padding(20.dp))

            Button(
                onClick = {
                    table =
                        if (filenameOrMethod.endsWith(".vcf", true) ||
                            filenameOrMethod.endsWith(".vcf.gz", true)
                        ) {
                            BuilderFromHaplotypeVCF().read(filenameOrMethod)
                        } else if (filenameOrMethod.endsWith(".hmp.txt", true) ||
                            filenameOrMethod.endsWith(".hmp.txt.gz", true)
                        ) {
                            BuilderFromHapMap.getBuilder(filenameOrMethod).build()
                        } else {
                            // Get feature table from BrAPI server:
                            // THe connection is stable, it is the db accessed through this connection that uses
                            // either phgsmallseq_test (for testing) or maize_1_0 when initial tests work)
                            //val connection = BrAPIConnection(selectedURL)
                            val connection = BrAPIConnection(urlbase.text)
                            connection.getCalls(filenameOrMethod)
                        }
                },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color.Blue,
                    contentColor = Color.White
                )
            ) {
                Text(
                    text = "Load",
                    modifier = Modifier.padding(10.dp)
                )
            }

        }

        Row(
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Column(modifier = Modifier.weight(1.0f)) {
                HaplotypeGrid(table)
            }
            // This is a space holder for adding data e.g. jbrowse on the screen with the haplotype grid
            Column(modifier = Modifier.weight(1.0f)) {
                //Text("TERRY")

            }
        }

    }
}

fun getPathList(baseURL:String): List<String>? {
    //val connection = BrAPIConnection(url)
    var client = OkHttpClient()
    var request = Request.Builder()
        .url(baseURL + "/variantSets/")
        .build()

    val response = client.newCall(request).execute()
    if (response != null) {
        // http://tutorials.jenkov.com/java-json/gson-jsonparser.html
        val responseData = response.body?.string()
        val gsonParser = JsonParser()
        val jsonTree = gsonParser.parse(responseData)
        val jsonObject = if (jsonTree.isJsonObject()) jsonTree.getAsJsonObject() else println("jsonTree not a json object !!")
        // TODO: Figure out how to parse this!

    }
    // FOr now, just create a string of path methods and return it
    println("getPathList: returning hard-coded path list")
    val pathList = listOf("B73Ref_method","GATK_PIPELINE","GATK_PIPELINE_PATH","mummer4","mummer4_PATH","PATH_METHOD","READS_FASTQ")

    return pathList

}