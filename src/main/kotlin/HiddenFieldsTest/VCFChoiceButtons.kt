package HiddenFIeldsTest

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import net.maizegenetics.analysis.brapi.BrAPIConnection
import net.maizegenetics.brapp.hapviewer.FileChooser
import net.maizegenetics.brapp.hapviewer.HaplotypeGrid
import net.maizegenetics.dna.factor.FeatureTable
import net.maizegenetics.dna.factor.io.BuilderFromHapMap
import net.maizegenetics.dna.factor.io.BuilderFromHaplotypeVCF

@Composable
fun VCFButtons() {

    var filenameOrMethod by remember { mutableStateOf("") }
    var isOpen = remember { mutableStateOf(false) }

    var table: FeatureTable? by remember { mutableStateOf(null) }

    var selectedURL by remember { mutableStateOf("http://cbsudc01.biohpc.cornell.edu/brapi/v2")}

    if (isOpen.value) {
        FileChooser(
            onCloseRequest = { filename ->
                filename?.let { filenameOrMethod = it }
                isOpen.value = false
                println("Result $filename")
            }
        )
    }

    Column {

        Spacer(modifier = Modifier.padding(20.dp))

        Row(verticalAlignment = Alignment.CenterVertically) {

            Spacer(modifier = Modifier.padding(20.dp))

            OutlinedTextField(
                value = filenameOrMethod,
                onValueChange = { filenameOrMethod = it },
                label = { Text("VCF file") }
            )

            Spacer(modifier = Modifier.padding(20.dp))

            Button(
                onClick = {
                    isOpen.value = true
                },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color.Blue,
                    contentColor = Color.White
                )
            ) {
                Text(
                    text = "Browse for File",
                    modifier = Modifier.padding(10.dp)
                )
            }

            Spacer(modifier = Modifier.padding(20.dp))

            Button(
                onClick = {
                    table =
                        if (filenameOrMethod.endsWith(".vcf", true) ||
                            filenameOrMethod.endsWith(".vcf.gz", true)
                        ) {
                            BuilderFromHaplotypeVCF().read(filenameOrMethod)
                        } else if (filenameOrMethod.endsWith(".hmp.txt", true) ||
                            filenameOrMethod.endsWith(".hmp.txt.gz", true)
                        ) {
                            BuilderFromHapMap.getBuilder(filenameOrMethod).build()
                        } else {
                            // Get feature table from BrAPI server:
                            // THe connection is stable, it is the db accessed through this connection that uses
                            // either phgsmallseq_test (for testing) or maize_1_0 when initial tests work)
                            val connection = BrAPIConnection(selectedURL)
                            connection.getCalls(filenameOrMethod)
                        }
                },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color.Blue,
                    contentColor = Color.White
                )
            ) {
                Text(
                    text = "Load",
                    modifier = Modifier.padding(10.dp)
                )
            }
        }

        Row(
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Column(modifier = Modifier.weight(1.0f)) {
                HaplotypeGrid(table)
            }
            // This is a space holder for adding data e.g. jbrowse on the screen with the haplotype grid
            Column(modifier = Modifier.weight(1.0f)) {
               // Text("TERRY")
            }
        }

    }
}