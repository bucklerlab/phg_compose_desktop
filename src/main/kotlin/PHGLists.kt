//import android.util.Log
import androidx.compose.desktop.Window
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.foundation.shape.RoundedCornerShape

import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
//import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Popup
import javax.swing.text.TableView

//import org.jetbrains.compose.common.foundation.layout.fillMaxHeight
//import org.jetbrains.compose.common.foundation.layout.offset
//import org.jetbrains.compose.common.internal.castOrCreate

/**
 * In this file I'm using androidx.compose* imports.  I need them as I'm
 * using LazyColumn and LazyRow - which is in androidx, not jetbrains.compose.
 * So we get Androidx Modifiers, and must use its constraints, including
 * how to extend a class to add position.
 */

// https://www.youtube.com/watch?v=Zpthg3lZeS0
// https://dev.to/agarasul/complex-ui-in-jetpack-compose-o41

// create PHG test data, use lazy column and lazyRow to print - they are both scrollable
@Composable
fun LazyColumnRefRanges() {
    val uriHandler = LocalUriHandler.current
    // create a list of reference ranges - for this test, only do 5 of them
    // When this is real, they should all be pulled from the db.
    val refRangeList = createTestRefRangeList()
    val haplotypesList = createTestHaplotypesList()
    val listForLazyColumns = createRefRangeHapListRow( refRangeList, haplotypesList)

    var rrButtonText by remember { mutableStateOf("click for ref range info") }
    var hapButtonText by remember { mutableStateOf("click for haplotype jbrowse window") }

    var rrQuery = remember{mutableStateOf("208")} // try savedInstanceState function
    var hapQuery = remember{mutableStateOf("76919")} // try savedInstanceState function

    // This assumes all taxa have an entry for all
    Column (verticalArrangement = Arrangement.Top){
        Row {
            Spacer(modifier = Modifier.padding(5.dp))
            TextField(
                value=rrQuery.value,
                // I want this to ONLY apply when use hits return, and
                // we know they are finished typing.  BUt typing in anything throws exceptions
                onValueChange = {newValue ->
                    rrQuery.value = newValue
                },
                label = {Text("Reference Range")},
                modifier = Modifier.align(Alignment.CenterVertically).size(140.dp,50.dp)
            )
            Spacer(modifier = Modifier.padding(5.dp))
            val openDialog = remember { mutableStateOf(false) }
            Button(modifier = Modifier.align(Alignment.CenterVertically),
                onClick = {
                    // This merely changes the button text, but instead it should
                    // pop up a text box with the reference range information
                    openDialog.value = true
                    var queryAsInt = -1
                    var chr = ""
                    var start = ""
                    var end = ""

                    try {
                        queryAsInt = rrQuery.value.toInt()
                        val refRangeMapById = refRangeList.groupBy{it.myID}
                        val refRangeData = refRangeMapById.get(queryAsInt)
                        if (refRangeData == null) {
                            rrButtonText = "Reference Range: ${queryAsInt} not in path"
                        } else {
                            chr = refRangeData!![0].myChromosome
                            start = refRangeData!![0].myStart.toString()
                            end = refRangeData!![0].myEnd.toString()
                            rrButtonText = "chr: ${chr}\nstart: ${start}\nend: ${end}"
                        }
                    } catch (nfe:NumberFormatException) {
                        rrButtonText = "Reference Range must be an integer"
                    }

                }) {
                Text(rrButtonText)
            }
        }
        Spacer(modifier = Modifier.padding(5.dp))
        Row {
            Spacer(modifier = Modifier.padding(5.dp))
            TextField(
                value=hapQuery.value,
                // I want this to ONLY apply when use hits return, and
                // we know they are finished typing.  BUt typing in anything throws exceptions
                onValueChange = {newValue ->
                    hapQuery.value = newValue
                },
                label = {Text("Haplotype ID")},
                modifier = Modifier.align(Alignment.CenterVertically).size(140.dp,50.dp)
            )
            Spacer(modifier = Modifier.padding(5.dp))
            Button(modifier = Modifier.align(Alignment.CenterVertically),
                onClick = {
                    // This merely changes the button text, but instead it should
                    // pop up a text box with the reference range information
                    var queryAsInt = -1
                    try {
                        queryAsInt = hapQuery.value.toInt()
                        val hapMapById = haplotypesList.groupBy{it.hapId}
                        val hapData = hapMapById.get(queryAsInt)
                        if (hapData == null) {
                            // How to pop up an error message?
                            hapButtonText = " haplotype id ${queryAsInt} not in path data"
                        } else {
                            val chr = "chr${hapData!![0].asmContig}"
                            val start = hapData!![0].asmStart
                            val end = hapData!![0].asmEnd
                            val taxon = hapData!![0].taxa
                            val url = "https://jbrowse.maizegdb.org/?data=${taxon}&loc=${chr}%3A${start}..${end}&tracks=gene_models_official%2Cgene_models&highlight="
                            uriHandler.openUri(url)

                        }
                    } catch (nfe:NumberFormatException) {
                        hapButtonText = " must enter an integer haplotype id"
                    }

                }) {
                Text(hapButtonText)
            }
        }

    }
}



