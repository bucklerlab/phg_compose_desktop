import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

/**
 * This file creates scrollable tables of PHG information.  It scrolls both rows and columns.
 * However ... the columns are LazyColumns, but the rows are not.  It would be good to have
 * them as LazyRows as that is more efficient - the rows entries only created as needed.
 *
 * Main table example comes from:
 *     https://stackoverflow.com/questions/68143308/how-can-i-create-a-table-in-jetpack-compose
 * Scroll example from here:
 *     https://proandroiddev.com/scrollable-topappbar-with-jetpack-compose-bf22ca900cfe
 */
@Composable
fun RowScope.TableCell(
    text: String,
    weight: Float,
    width: Dp,
    height: Dp
) {
    Text(
        text = text,
        Modifier
            .border(1.dp, Color.Black)
            //.weight(weight)
            .padding(8.dp)
            .size(width,height)
    )
}

@Composable
fun TableScreen() {
    val uriHandler = LocalUriHandler.current
    // create a list of reference ranges - for this test, only do 5 of them
    // When this is real, they should all be pulled from the db.
    val refRangeList = createTestRefRangeList()
    val haplotypeList = createTestHaplotypesList()
    val listForLazyColumns = createRefRangeHapListRow( refRangeList, haplotypeList)
    val haplotypeMapByTaxa = haplotypeList.groupBy {it.taxa}

    // THe listForLazyColumns has tab-delimited string of <refRangId><hapid-taxa1><hapid-taxa2> ... <hapid-taxaN>
    // THe header row is already on here.
    // Just a fake data... a Pair of Int and String

    // Each cell of a column must have the same weight.
    val column1Weight = .2f // 30%
    val scrollState = rememberScrollState()
    // The LazyColumn will be our table. "weight" option divides the available space
    //  to be shared by each element in the row.  That has been commented out and I
    //  added "size" instead to keep the cell size consistent, and allow for scrolling

    LazyColumn(Modifier.width(500.dp).padding(16.dp)) {
        // Here is the header - 1st column is ref range id, other columns are taxa
        item {
            Row(Modifier.background(Color.Gray).horizontalScroll(scrollState)) {
                val headerCells = listForLazyColumns.get(0).split("\t")
                for (entry in headerCells) {
                    TableCell(text = entry, weight = column1Weight, width = 100.dp, height = 35.dp)
                }
            }
        }
        // Here are all the data lines of the table. (each entry is a haplotype id)
        val justData = listForLazyColumns.drop(1)
        items(justData) {
            val data = it
            Row(Modifier.fillMaxWidth().horizontalScroll(scrollState)) {
                val hapList = data.split("\t")
                for (hap in hapList) {
                    TableCell(text = hap, weight = column1Weight, width = 100.dp, height = 35.dp)
                }
            }
        }
    }

}