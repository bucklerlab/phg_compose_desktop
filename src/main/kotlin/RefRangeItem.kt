class RefRangeItem (var name: String, var chrom: String, var start: Int, var end: Int) {
    var hapids: ArrayList<Int>

    fun setHapids ( haps: List<Int>) {
        hapids.addAll(haps)
    }

    init {
        hapids = ArrayList<Int>()
    }
}

