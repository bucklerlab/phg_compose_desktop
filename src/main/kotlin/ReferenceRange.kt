/**
 * Class for basic ref range data - it might later take functions, so not yet reverting
 * to a data class
 */
data class ReferenceRange( val myID: Int, val myChromosome: String, val myStart: Int, val myEnd: Int) {
}