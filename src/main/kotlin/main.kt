import HiddenFIeldsTest.URLButtons
import HiddenFIeldsTest.VCFButtons
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.*
import net.maizegenetics.dna.factor.FeatureTable
import net.maizegenetics.util.setupDebugLogging
//import jdk.nashorn.internal.objects.NativeFunction.function
import java.util.*

// This creates table in separate window based on user input - used in Aug 2, 2021 demo
// Still need to determine way to get this on the same screen.
@OptIn(ExperimentalComposeUiApi::class)
//fun main() = application {
fun mainOrig() = application {
    setupDebugLogging()

    val filename = "/Users/lcj34/notes_files/phg_2018/jetpack_compose/nam_merged_haplotypes_chr10.vcf.gz"
    // If we figure out how to get the HaplotypeGrid on the same screen as the buttons,
    // then the image should be removed as we'll want that real-estate.
    Window(onCloseRequest = ::exitApplication,title = "Haplotype Viewer BrAPP",) {
        Column {
//            Image(
//                    bitmap = imageResource(("images/Aurora_PondAndField.jpg")),
//                    contentDescription = "Aurora Farm",
//                    modifier = Modifier.align(Alignment.CenterHorizontally)
//                )

            // THis now works with the haplotype grid appearing in the same window as the buttons.
            // The code is not actually needed as we'll use Terry's main, but I wanted the fixed
            // version of LCJ play code to in the repository.
            featureTableFromButtons()
        }

    }
}

fun main() = application {
//fun mainTestHidden() = application {
    setupDebugLogging()

    val state = rememberWindowState(size = WindowSize(1920.dp, 1080.dp), position = WindowPosition(Alignment.Center))

    val filename = "/Users/lcj34/notes_files/phg_2018/jetpack_compose/nam_merged_haplotypes_chr10.vcf.gz"
    // If we figure out how to get the HaplotypeGrid on the same screen as the buttons,
    // then the image should be removed as we'll want that real-estate.
    Window(onCloseRequest = ::exitApplication,title = "Haplotype Viewer BrAPP",state = state) {

        var vcfScreenHidden by remember {mutableStateOf(true)}
        var urlScreenHidden by remember {mutableStateOf(true)}
        var selectedText by remember { mutableStateOf("Grid from VCF File")}
        val formatOptions = listOf("Grid from VCF File", "Grid from URL")

        Column {
            // THis now works with the haplotype grid appearing in the same window as the buttons.
            // The code is not actually needed as we'll use Terry's main, but I wanted the fixed
            // version of LCJ play code to in the repository.
            var expandedFileOrURL by remember { mutableStateOf(false) }

            OutlinedTextField(
                value = selectedText,
                onValueChange = { selectedText = it },
                //modifier = Modifier.fillMaxWidth(),
                label = {Text("Feature Table Input Option")},
                trailingIcon = {
                    Icon(Icons.Default.ArrowDropDown, contentDescription = null, modifier = Modifier.clickable { expandedFileOrURL = !expandedFileOrURL })
                    //Icon(icon,"contentDescription", Modifier.clickable { expanded = !expanded })
                }
            )
            DropdownMenu(
                expanded = expandedFileOrURL,
                onDismissRequest = { expandedFileOrURL = false },
                modifier = Modifier.background(Color.LightGray).align(Alignment.CenterHorizontally).weight(2f)
            ) {
                formatOptions.forEach { label ->
                    DropdownMenuItem(onClick = {
                        selectedText = label
                        println("in onclick - label=${label}, selectedText=${selectedText}")
                        if (label.equals("Grid from VCF File")) {
                            println("setting vcfScreenHidden to true via label")
                            vcfScreenHidden=false
                            urlScreenHidden = true
                        } else {
                            vcfScreenHidden=true
                            urlScreenHidden=false
                            println("setting vcfScreenHidden FALSE via label")
                        }
                    }) {
                        Text(text = label)
                    }
                }
            } // end DropDownMenu

            if (!vcfScreenHidden) {
                VCFButtons()
            }
            if (!urlScreenHidden) {
                URLButtons()
            }
        }

    }
}
