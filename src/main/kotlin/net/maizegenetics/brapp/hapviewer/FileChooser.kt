package net.maizegenetics.brapp.hapviewer

import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.window.AwtWindow
import androidx.compose.ui.window.application
import java.awt.FileDialog
import java.awt.Frame

@Composable
fun FileChooser(
    parent: Frame? = null,
    onCloseRequest: (result: String?) -> Unit
) = AwtWindow(
    create = {
        object : FileDialog(parent, "Choose a file", LOAD) {
            override fun setVisible(value: Boolean) {
                super.setVisible(value)
                if (value) {
                    onCloseRequest("$directory$file")
                }
            }
        }
    },
    dispose = FileDialog::dispose
)

@OptIn(ExperimentalComposeUiApi::class)
fun main() = application {
    var isOpen = remember { mutableStateOf(true) }

    if (isOpen.value) {
        FileChooser(
            onCloseRequest = {
                isOpen.value = false
                println("Result $it")
            }
        )
    }
}