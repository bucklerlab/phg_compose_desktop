package net.maizegenetics.brapp.hapviewer

import net.maizegenetics.dna.factor.FeatureTable
import net.maizegenetics.dna.factor.FeatureTableBuilder
import net.maizegenetics.dna.factor.site.HaplotypeSite
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.dna.map.GenomicFeature
import net.maizegenetics.dna.map.GenomicFeatureList
import net.maizegenetics.taxa.TaxaListBuilder
import kotlin.random.Random

fun testFeatureTable(numTaxa: Int = 10, numFeatures: Int = 100): FeatureTable {

    val taxa = TaxaListBuilder.getInstance(numTaxa)
    val sites = GenomicFeatureList.Builder()
    val chr = Chromosome.instance(1)
    var start = 1
    (0 until numFeatures).forEach {
        val gf = GenomicFeature(chr, start, chr, start + 999)
        sites.add(gf)
        start += 1000
    }
    val builder = FeatureTableBuilder(taxa, sites.build(), HaplotypeSite::class)
    val random = Random(System.nanoTime())
    for (t in 0 until numTaxa) {
        for (s in 0 until numFeatures) {
            val first = "s${s}_${random.nextInt(10)}"
            val second = "s${s}_${random.nextInt(10)}"
            builder.set(t, s, listOf(first, second))
        }
    }

    return builder.build()

}