package net.maizegenetics.brapp.hapviewer

import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ExperimentalGraphicsApi
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import net.maizegenetics.brapp.hapviewer.color.AssemblyColorScheme
import net.maizegenetics.dna.factor.FeatureTable
import net.maizegenetics.dna.factor.UNKNOWN_ALLELE
import net.maizegenetics.dna.factor.site.HaplotypeSite
import kotlin.math.abs
import kotlin.math.roundToInt


val colorScheme = AssemblyColorScheme()

const val minCellSize = 6
const val maxCellSize = 40
const val dividerLineWidth = 1


@OptIn(ExperimentalGraphicsApi::class)
@Composable
fun HaplotypeGrid(featureTable: FeatureTable?) {

    if (featureTable == null) return

    val uriHandler = LocalUriHandler.current

    Column {

        val scaleScrollState = rememberScrollState(initial = (Int.MAX_VALUE.toDouble() * 0.4).roundToInt())

        Spacer(modifier = Modifier.padding(10.dp))

        Row(verticalAlignment = Alignment.CenterVertically) {

            Spacer(modifier = Modifier.padding(20.dp))

            Text("Scale:", fontSize = 20.sp)

            Spacer(modifier = Modifier.padding(10.dp))

            HorizontalScrollbar(
                adapter = ScrollbarAdapter(scaleScrollState),
                style = ScrollbarStyle(
                    minimalHeight = 15.dp,
                    thickness = 15.dp,
                    shape = CircleShape,
                    hoverDurationMillis = 0,
                    unhoverColor = Color.Black.copy(alpha = 0.5f),
                    hoverColor = Color.Black.copy(alpha = 0.5f)
                ),
                modifier = Modifier
                    .width(300.dp)
                    .border(width = 1.dp, color = Color.Black, shape = CircleShape)
                    .align(Alignment.CenterVertically)
            )

        }

        Spacer(modifier = Modifier.padding(10.dp))

        val horizontalScrollState = rememberScrollState()

        var canvasSize by remember { mutableStateOf(Size(0.0f, 0.0f)) }

        var cellSize = calculateCellSize(scaleScrollState.value, scaleScrollState.maxValue)

        var numColumns = numOfColumns(canvasSize, cellSize, featureTable.numFeatures())
        var numRows = numOfRows(canvasSize, cellSize, featureTable.numTaxa())

        var startSite = calculateStartSite(
            featureTable.numFeatures(),
            horizontalScrollState.value,
            horizontalScrollState.maxValue,
            numColumns
        )

        var taxaLabelWidth = cellSize / 2.1 * 7.0

        var rowHeight = cellSize / 2.0 * 13.0

        Row(verticalAlignment = Alignment.CenterVertically) {

            val headingPadding = (14 + taxaLabelWidth * 0.55).dp
            Spacer(modifier = Modifier.padding(horizontal = headingPadding, vertical = 1.dp))

            HorizontalScrollbar(
                adapter = ScrollbarAdapter(horizontalScrollState),
                style = ScrollbarStyle(
                    minimalHeight = 80.dp,
                    thickness = 15.dp,
                    shape = CircleShape,
                    hoverDurationMillis = 0,
                    unhoverColor = Color.Black.copy(alpha = 0.5f),
                    hoverColor = Color.Black.copy(alpha = 0.5f)
                ),
                modifier = Modifier
                    .fillMaxWidth(0.98f)
                    .border(width = 1.dp, color = Color.Black, shape = CircleShape)
                    .align(Alignment.CenterVertically)
            )

        }

        Spacer(modifier = Modifier.padding(5.dp))

        Row(
            horizontalArrangement = Arrangement.Center,
            modifier = Modifier
                .fillMaxWidth()
                .height(rowHeight.dp)
        ) {

            val headingPadding = (14 + taxaLabelWidth * 0.55).dp
            Spacer(modifier = Modifier.padding(headingPadding))
            columnHeader(cellSize, startSite, numColumns, featureTable, canvasSize)

        }

        Spacer(modifier = Modifier.padding(1.dp))

        Row {

            Spacer(modifier = Modifier.padding(5.dp))

            val verticalScrollState = rememberScrollState()

            VerticalScrollbar(
                adapter = ScrollbarAdapter(verticalScrollState),
                style = ScrollbarStyle(
                    minimalHeight = 80.dp,
                    thickness = 15.dp,
                    shape = CircleShape,
                    hoverDurationMillis = 0,
                    unhoverColor = Color.Black.copy(alpha = 0.5f),
                    hoverColor = Color.Black.copy(alpha = 0.5f)
                ),
                modifier = Modifier
                    .fillMaxHeight(0.95f)
                    .border(width = 1.dp, color = Color.Black, shape = CircleShape)
                    .align(Alignment.CenterVertically)
            )

            Spacer(modifier = Modifier.padding(3.dp))

            var startTaxon = calculateStartTaxon(
                featureTable.numTaxa(),
                verticalScrollState.value,
                verticalScrollState.maxValue,
                numRows
            )

            Column {

                val cellSizePlusDivider = (cellSize + dividerLineWidth).toFloat()

                for (t in startTaxon until startTaxon + numRows) {
                    println("Taxa Label: cellSize: $cellSize")
                    Text(
                        text = featureTable.taxa[t].name,
                        fontSize = (cellSize / 2.1).sp,
                        textAlign = TextAlign.Center,
                        modifier = Modifier
                            .requiredHeight((cellSizePlusDivider / 2.0).dp)
                            .requiredWidth(taxaLabelWidth.dp)
                    )
                }

            }

            Spacer(modifier = Modifier.padding(3.dp))

            Canvas(
                modifier = Modifier.fillMaxSize()
                    .pointerInput(Unit) {
                        detectTapGestures(
                            onPress = {
                                println("canvasSize: $canvasSize")

                                val cellSizeRecalculate = calculateCellSize(
                                    scaleScrollState.value,
                                    scaleScrollState.maxValue
                                )

                                val numColumns = numOfColumns(canvasSize, cellSizeRecalculate, featureTable.numFeatures())
                                val numRows = numOfRows(canvasSize, cellSizeRecalculate, featureTable.numTaxa())

                                val startSiteRecalculate = calculateStartSite(
                                    featureTable.numFeatures(),
                                    horizontalScrollState.value,
                                    horizontalScrollState.maxValue,
                                    numColumns
                                )
                                val startTaxonRecalculate = calculateStartTaxon(
                                    featureTable.numTaxa(),
                                    verticalScrollState.value,
                                    verticalScrollState.maxValue,
                                    numRows
                                )

                                val clickedTaxon = it.y.toInt() / cellSizeRecalculate + startTaxonRecalculate
                                val clickedSite = it.x.toInt() / cellSizeRecalculate + startSiteRecalculate
                                println("press x: ${it.x}  y: ${it.y}")
                                println("clickedSite: ${clickedSite}  clickedTaxon: ${clickedTaxon}")

                                val site = featureTable[clickedSite]
                                val taxon = featureTable.taxa[clickedTaxon]

                                val allele0 = site.genotype(clickedTaxon)[0]
                                if (allele0 != UNKNOWN_ALLELE) {
                                    val annotation = (site as HaplotypeSite).haplotypeAnnotation(allele0)

                                    annotation?.let {
                                        if (annotation.taxon.endsWith("_Assembly")) {
                                            val asmName = annotation.taxon.substringBefore("_Assembly")
                                            val chr = annotation.asmContig
                                            val start = annotation.asmStart
                                            val end = annotation.asmEnd
                                            val url =
                                                "https://jbrowse.maizegdb.org/?data=${asmName}&loc=chr${chr}%3A${start}..${end}&tracks=gene_models_official%2Cgene_models&highlight="
                                            uriHandler.openUri(url)
                                        }
                                    }

                                }

                            }
                        )
                    }
            ) {

                canvasSize = size

                println("cellSize: $cellSize  numColumns: $numColumns  numRows: $numRows  startSite: $startSite  startTaxon: $startTaxon")

                drawIntoCanvas {

                    drawRect(Color(red = 255, green = 255, blue = 255))

                    val cellSizePlusDivider = (cellSize + dividerLineWidth).toFloat()

                    for (xIndex in startSite until (startSite + numColumns).coerceAtMost(featureTable.numFeatures())) {
                        val site = featureTable[xIndex]
                        val x = (xIndex - startSite).toFloat() * cellSizePlusDivider + dividerLineWidth.toFloat()

                        for (yIndex in startTaxon until (startTaxon + numRows).coerceAtMost(featureTable.numTaxa())) {

                            val y =
                                (yIndex - startTaxon).toFloat() * cellSizePlusDivider + dividerLineWidth.toFloat()

                            val colors = colorScheme.color(site, yIndex)

                            drawRect(
                                color = colors[0],
                                Offset(x, y),
                                Size(cellSize.toFloat(), cellSize.toFloat() / 2.0f),
                                //style = Stroke(width = 1.0f)
                            )

                            drawRect(
                                color = colors[1],
                                Offset(x, y + cellSize / 2.0f),
                                Size(cellSize.toFloat(), cellSize.toFloat() / 2.0f)
                            )

                        }

                    }

                    for (yIndex in 0..numRows) {
                        val y = yIndex.toFloat() * cellSizePlusDivider + dividerLineWidth.toFloat()
                        drawLine(
                            color = Color.White,
                            start = Offset(0.0f, y),
                            end = Offset(size.width - 1.0f, y),
                            strokeWidth = dividerLineWidth.toFloat()
                        )
                    }

                    for (xIndex in 0..numColumns) {
                        val x = xIndex.toFloat() * cellSizePlusDivider + dividerLineWidth.toFloat()
                        drawLine(
                            color = Color.White,
                            start = Offset(x, 0.0f),
                            end = Offset(x, size.width - 1.0f),
                            strokeWidth = dividerLineWidth.toFloat()
                        )
                    }

                }

            }

        }

        Spacer(modifier = Modifier.padding(15.dp))

    }

}

@Composable
private fun columnHeader(cellSize: Int, startSite: Int, numColumns: Int, featureTable: FeatureTable, canvasSize: Size) {

    Column(
        modifier = Modifier
            .requiredHeight((canvasSize.width / 2.0f).dp)
            .rotate(-90f),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.Center
    ) {

        val cellSizePlusDivider = (cellSize + dividerLineWidth).toFloat()

        for (s in startSite until startSite + numColumns) {
            val feature = featureTable[s].feature
            Text(
                text = "${feature.startChr}:${feature.startPos}-${feature.endPos}",
                fontSize = (cellSize / 2.1).sp,
                modifier = Modifier
                    .requiredHeight((cellSizePlusDivider / 2.0).dp),
                maxLines = 1,
                textAlign = TextAlign.Left
            )
        }

    }

}

private fun calculateCellSize(scaleLocation: Int, scaleMax: Int): Int {
    println("scaleLoc: $scaleLocation  scaleMax: $scaleMax")
    return (((maxCellSize - minCellSize).toFloat() * (scaleLocation.toFloat() / scaleMax.toFloat()) + minCellSize) / 2.0f).roundToInt() * 2
}

private fun calculateStartSite(numFeatures: Int, horizontalLocation: Int, horizontalMax: Int, numOfColumns: Int): Int {
    return ((numFeatures.toDouble() - numOfColumns.toDouble()) * abs(horizontalLocation.toDouble() / horizontalMax.toDouble())).roundToInt()
        .coerceAtMost(numFeatures - numOfColumns)
}

private fun calculateStartTaxon(numTaxa: Int, verticalLocation: Int, verticalMax: Int, numOfRows: Int): Int {
    println("numTaxa: $numTaxa  numRows: $numOfRows  location: $verticalLocation  max: $verticalMax")
    return ((numTaxa.toDouble() - numOfRows.toDouble()) * abs(verticalLocation.toDouble() / verticalMax.toDouble())).roundToInt()
        .coerceAtMost(numTaxa - numOfRows)
}

// Don't let number of columns exceed the number of features
private fun numOfColumns(canvasSize: Size, cellSize: Int, numFeatures: Int): Int {
    val cellSizePlusDivider = (cellSize + dividerLineWidth).toFloat()
    return ((canvasSize.width - dividerLineWidth.toFloat()) / cellSizePlusDivider).roundToInt().coerceAtMost(numFeatures)
}

// Don't let number of rows exceed the number of taxa
private fun numOfRows(canvasSize: Size, cellSize: Int, numTaxa: Int): Int {
    val cellSizePlusDivider = (cellSize + dividerLineWidth).toFloat()
    return ((canvasSize.height - dividerLineWidth.toFloat()) / cellSizePlusDivider).roundToInt().coerceAtMost(numTaxa)
}