package net.maizegenetics.brapp.hapviewer

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.*
import net.maizegenetics.analysis.brapi.BrAPIConnection
import net.maizegenetics.dna.factor.FeatureTable
import net.maizegenetics.dna.factor.io.BuilderFromHapMap
import net.maizegenetics.dna.factor.io.BuilderFromHaplotypeVCF
import net.maizegenetics.util.setupDebugLogging

@OptIn(ExperimentalComposeUiApi::class)
fun main() = application {

    setupDebugLogging()

    val state = rememberWindowState(size = WindowSize(1920.dp, 1080.dp), position = WindowPosition(Alignment.Center))

    //val filename = "/Users/tmc46/phg/kinship/nam_merged_haplotypes_chr10.vcf.gz"
    //val featureTable = BuilderFromHaplotypeVCF().read(filename)

    Window(onCloseRequest = ::exitApplication, title = "Haplotype Viewer BrAPP", state = state) {

        var expanded by remember { mutableStateOf(false) }
        var filenameOrMethod by remember { mutableStateOf("") }
        var isOpen = remember { mutableStateOf(false) }

        var table: FeatureTable? by remember { mutableStateOf(null) }

        // The first URL option connects to the maize_1_0 db on dc01
        // The second URL option allows the user (lcj34/tmc46) to connect to the phgsmallseq_test db,
        // using their local machine to connect to cbsudc01.
        val urlOptions = listOf("http://cbsudc01.biohpc.cornell.edu/brapi/v2", "http://localhost:8080/brapi/v2")

        var selectedURL by remember { mutableStateOf("http://cbsudc01.biohpc.cornell.edu/brapi/v2")}

        if (isOpen.value) {
            FileChooser(
                onCloseRequest = { filename ->
                    filename?.let { filenameOrMethod = it }
                    isOpen.value = false
                    println("Result $filename")
                }
            )
        }

        Column {

            Spacer(modifier = Modifier.padding(20.dp))

            Row(verticalAlignment = Alignment.CenterVertically) {

                Spacer(modifier = Modifier.padding(20.dp))

                OutlinedTextField(
                    value = filenameOrMethod,
                    onValueChange = { filenameOrMethod = it },
                    label = { Text("Filename / Method") }
                )

                Spacer(modifier = Modifier.padding(20.dp))

                Button(
                    onClick = {
                        isOpen.value = true
                    },
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = Color.Blue,
                        contentColor = Color.White
                    )
                ) {
                    Text(
                        text = "Browse for File",
                        modifier = Modifier.padding(10.dp)
                    )
                }

                Spacer(modifier = Modifier.padding(20.dp))
                // THe column here lines up the drop-down menu options with the drop-down button.
                // without it, the options appear at the far left.
                Column() {
                    OutlinedTextField(
                        value = selectedURL,
                        onValueChange = { selectedURL = it },
                        //modifier = Modifier.fillMaxWidth(),
                        label = {Text("BrAPI URL Options")},
                        trailingIcon = {
                            Icon(Icons.Default.ArrowDropDown, contentDescription = null, modifier = Modifier.clickable { expanded = !expanded })
                            //Icon(icon,"contentDescription", Modifier.clickable { expanded = !expanded })
                        }
                    )
                    DropdownMenu(
                        expanded = expanded,
                        onDismissRequest = { expanded = false },
                        modifier = Modifier.background(Color.LightGray).align(Alignment.CenterHorizontally).weight(2f)
                    ) {
                        urlOptions.forEach { label ->
                            DropdownMenuItem(onClick = {
                                selectedURL = label
                                println("in onclick - label=${label}, selectedText=${selectedURL}")
                            }) {
                                Text(text = label)
                                println("After suggestions in DropdownMenu: label= ${label}")
                            }
                        }
                    }
                }

                Spacer(modifier = Modifier.padding(20.dp))

                Button(
                    onClick = {
                        table =
                            if (filenameOrMethod.endsWith(".vcf", true) ||
                                filenameOrMethod.endsWith(".vcf.gz", true)
                            ) {
                                BuilderFromHaplotypeVCF().read(filenameOrMethod)
                            } else if (filenameOrMethod.endsWith(".hmp.txt", true) ||
                                filenameOrMethod.endsWith(".hmp.txt.gz", true)
                            ) {
                                BuilderFromHapMap.getBuilder(filenameOrMethod).build()
                            } else {
                                // Get feature table from BrAPI server:
                                // THe connection is stable, it is the db accessed through this connection that uses
                                // either phgsmallseq_test (for testing) or maize_1_0 when initial tests work)
                                val connection = BrAPIConnection(selectedURL)
                                connection.getCalls(filenameOrMethod)
                            }
                    },
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = Color.Blue,
                        contentColor = Color.White
                    )
                ) {
                    Text(
                        text = "Load",
                        modifier = Modifier.padding(10.dp)
                    )
                }

            }

            Row(
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(modifier = Modifier.weight(1.0f)) {
                    HaplotypeGrid(table)
                }
                // This is a space holder for adding data e.g. jbrowse on the screen with the haplotype grid
                Column(modifier = Modifier.weight(1.0f)) {
                    Text("TERRY")
                }
            }

        }

    }

}
