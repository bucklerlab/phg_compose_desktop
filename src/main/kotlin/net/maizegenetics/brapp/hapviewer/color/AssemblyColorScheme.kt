package net.maizegenetics.brapp.hapviewer.color

import androidx.compose.ui.graphics.Color
import net.maizegenetics.dna.factor.UNKNOWN_ALLELE
import net.maizegenetics.dna.factor.site.FeatureSite
import net.maizegenetics.dna.factor.site.HaplotypeSite

class AssemblyColorScheme : ColorScheme {

    private val assemblyToColor = mutableMapOf<String, Color>()
    private var currentIndex = 0

    override fun color(site: FeatureSite, taxon: Int): Array<Color> {
        return site.genotype(taxon)
            .map { allele ->
                if (allele == UNKNOWN_ALLELE) Color.Gray
                else (site as HaplotypeSite).haplotypeAnnotation(allele).let { annotation ->
                    if (annotation == null) Color.DarkGray
                    else if (annotation.taxon == null || annotation.taxon.isEmpty()) Color.DarkGray
                    else {
                        var result = assemblyToColor[annotation.taxon]
                        if (result == null) {
                            println("adding ${annotation.taxon} as $currentIndex")
                            result = DISTINCT_COLORS[currentIndex++]
                            assemblyToColor[annotation.taxon] = result
                        }
                        result
                    }
                }
            }
            .toTypedArray()
    }

}