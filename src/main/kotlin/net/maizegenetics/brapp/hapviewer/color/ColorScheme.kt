package net.maizegenetics.brapp.hapviewer.color

import androidx.compose.ui.graphics.Color
import net.maizegenetics.dna.factor.site.FeatureSite

interface ColorScheme {

    fun color(site: FeatureSite, taxon: Int): Array<Color>

}